package fleetlib

import (
	"strings"
	"time"

	"github.com/rs/xid"
)

var _ Vehicle = (*Scooter)(nil)

// Scooter defines the data model for the a scooter.
type Scooter struct {
	serialNumber string
	category     string
	state
}

// state defines the data model for the state of a scooter.
type state struct {
	stateName    string
	operational  bool
	batteryLevel int
	lastModified time.Time
}

// NewScooter instantiates a new scooter.
func NewScooter() *Scooter {
	return &Scooter{
		serialNumber: strings.ToUpper(xid.New().String()),
		category:     categoryScooter,
		state: state{
			stateName:    stateServiceMode,
			operational:  operationalState[stateServiceMode],
			batteryLevel: batteryMaxCap,
			lastModified: time.Now().In(time.UTC),
		},
	}
}

// BatteryLevel returns the scooter's battery level.
func (s *Scooter) BatteryLevel() int {
	return s.batteryLevel
}

// Category returns the scooter's category.
func (s *Scooter) Category() string {
	return s.category
}

// LastModified returns the time of the last state change.
func (s *Scooter) LastModified() time.Time {
	return s.lastModified
}

// Operational returns the scooter's Operational state.
func (s *Scooter) Operational() bool {
	return s.operational
}

// SerialNumber returns the scooter's serial number.
func (s *Scooter) SerialNumber() string {
	return s.serialNumber
}

// SetAutoBounty bypass the transition rules and sets the scooter state to "bounty".
func (s *Scooter) SetAutoBounty() {
	s.setState(stateBounty)
}

// SetAutoUnknown bypass the transition rules and sets the scooter state to "unknown".
func (s *Scooter) SetAutoUnknown() {
	s.setState(stateUnknown)
}

// SetRandState is a convenience method that sets the scooter to a random state.
// NOTE: Doesn't have to be random, unpredictable is good enough.
func (s *Scooter) SetRandState() {
	for state := range operationalState {
		s.setState(state)
		break
	}
}

// SetState sets the a scooter to a new state.
func (s *Scooter) SetState(userRole, newState string) error {
	if !s.authorizedStateTransition(userRole) {
		return ErrUnauthorizedUserRole
	}

	if _, found := operationalState[newState]; !found {
		return ErrUnsupportedVehicleState
	}

	if userRole == UserRoleAdmin {
		s.setState(newState)
		return nil

	}

	if !s.validateStateTransition(newState) {
		return ErrUnprocessableVehicleStateTransition
	}

	switch newState {
	case stateRiding:
		// We assume that the battery is consumed every time
		// someone uses the scooter. Meaning that if the battery
		// level drops bellow 20% the state will be  set to "bounty"
		// since "battery_low" has an automatic transition.
		s.setState(stateRiding)
		if err := s.consumeBattery(); err != nil {
			return err
		}
	case stateBatteryLow:
		// "battery_low" is a transient state with automatic
		// transition to "bounty". Only admins can use "battery_low"
		// as a steady state.
		s.setState(stateBounty)
	case stateDropped:
		s.chargeBattery()
		s.setState(stateDropped)
	default:
		s.setState(newState)
	}
	return nil
}

// setState convenience method to set the state.
func (s *Scooter) setState(state string) {
	s.stateName = state
	s.operational = operationalState[state]
	s.lastModified = time.Now().In(time.UTC)
}

// authorizedStateTransition checks if user can modify scooter current state.
func (s *Scooter) authorizedStateTransition(userRole string) bool {
	allowedStates, _ := UserRoleAllowedVehicleStates(userRole)
	for _, allowedState := range allowedStates {
		if s.stateName == allowedState {
			return true
		}
	}
	return false
}

// validateStateTransition validates the transition from
// the current to the next transition.
func (s *Scooter) validateStateTransition(newState string) bool {
	return stateTransition[s.stateName] == newState
}

// chargeBattery charges the battery to its full capacity.
func (s *Scooter) chargeBattery() {
	s.batteryLevel = batteryMaxCap
}

// consumeBattery consumes the battery charge in 10%
// and sets the scooter state to "bounty" in case the level
// drops bellow 20%. The assumption is that the battery charge
// decreases 10% on each run and battery low state overwrites any current state.
func (s *Scooter) consumeBattery() error {
	if s.batteryLevel < batteryRunUsage {
		s.setState(stateBounty)
		return ErrVehicleBatteryLevelTooLow
	}

	s.batteryLevel -= batteryRunUsage

	if s.batteryLevel < batteryLowLvl {
		s.setState(stateBounty)
	}
	return nil
}

// State returns the scooter's serial number.
func (s *Scooter) State() string {
	return s.stateName
}
