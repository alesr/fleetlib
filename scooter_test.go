package fleetlib

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestNewScooter(t *testing.T) {
	t.Parallel()

	expectedStateName := stateServiceMode
	expectedOperational := operationalState[stateServiceMode]
	expectedBatteryLvl := batteryMaxCap

	actual := NewScooter()

	assert.NotEmpty(t, actual.SerialNumber)
	assert.Equal(t, expectedStateName, actual.stateName)
	assert.Equal(t, expectedOperational, actual.operational)
	assert.Equal(t, expectedBatteryLvl, actual.batteryLevel)
}

func TestBatteryLevel(t *testing.T) {
	t.Parallel()

	expectedBatteryLevel := batteryMaxCap
	scooter := NewScooter()

	actual := scooter.BatteryLevel()

	assert.Equal(t, expectedBatteryLevel, actual)
}

func TestCategory(t *testing.T) {
	t.Parallel()

	expectedCategory := categoryScooter
	scooter := NewScooter()

	actual := scooter.Category()

	assert.Equal(t, expectedCategory, actual)
}

func TestLastModified(t *testing.T) {
	t.Parallel()

	scooter := NewScooter()
	expectedLastModified := scooter.lastModified

	actual := scooter.LastModified()

	assert.Equal(t, expectedLastModified, actual)
}

func TestOperational(t *testing.T) {
	t.Parallel()

	expectedOperational := operationalState[stateServiceMode] // Default == false
	scooter := NewScooter()

	actual := scooter.Operational()

	assert.Equal(t, expectedOperational, actual)
}

func TestSerialNumber(t *testing.T) {
	t.Parallel()

	scooter := NewScooter()

	actual := scooter.SerialNumber()

	assert.NotEmpty(t, actual) // unpredictable
}

func TestSetAutoBounty(t *testing.T) {
	t.Parallel()

	expectedStateName := stateBounty
	expectedOperational := operationalState[stateBounty]

	scooter := NewScooter()

	scooter.SetAutoBounty()

	actualStateName := scooter.stateName
	actualOperational := scooter.operational

	assert.Equal(t, expectedStateName, actualStateName)
	assert.Equal(t, expectedOperational, actualOperational)
}

func TestSetAutoUnknown(t *testing.T) {
	t.Parallel()

	expectedStateName := stateUnknown
	expectedOperational := operationalState[stateUnknown]

	scooter := NewScooter()

	scooter.SetAutoUnknown()

	actualStateName := scooter.stateName
	actualOperational := scooter.operational

	assert.Equal(t, expectedStateName, actualStateName)
	assert.Equal(t, expectedOperational, actualOperational)
}

func TestSetRandState(t *testing.T) {
	t.Parallel()

	expectedNewStateIn := []string{
		stateReady,
		stateBatteryLow,
		stateBounty,
		stateRiding,
		stateCollected,
		stateDropped,
		stateServiceMode,
		stateTerminated,
		stateUnknown,
	}

	scooter := NewScooter()

	scooter.SetRandState()

	actualStateName := scooter.stateName
	actualStateOperational := scooter.operational

	assert.Contains(t, expectedNewStateIn, actualStateName)
	assert.Equal(t, operationalState[actualStateName], actualStateOperational)
}

func TestSetState(t *testing.T) {
	t.Parallel()

	defaultBatteryLvl := 50

	cases := []struct {
		name                     string
		givenOriginalState       string
		givenUserRole            string
		givenNewState            string
		expectedStateName        string
		expectedStateOperational bool
		expectedStateBatteryLvl  int
		expectedErr              error
	}{
		{
			name:                     "UnauthorizedUserRole",
			givenOriginalState:       stateServiceMode,
			givenUserRole:            "userRoleFoo",
			givenNewState:            stateReady,
			expectedStateName:        stateServiceMode,
			expectedStateOperational: false,
			expectedStateBatteryLvl:  defaultBatteryLvl,
			expectedErr:              ErrUnauthorizedUserRole,
		},
		{
			name:                     "UnsupportedState",
			givenOriginalState:       stateServiceMode,
			givenUserRole:            UserRoleAdmin,
			givenNewState:            "stateFoo",
			expectedStateName:        stateServiceMode,
			expectedStateOperational: false,
			expectedStateBatteryLvl:  defaultBatteryLvl,
			expectedErr:              ErrUnsupportedVehicleState,
		},
		{
			name:                     "adminSetsToAnyValidState",
			givenOriginalState:       stateServiceMode,
			givenUserRole:            UserRoleAdmin,
			givenNewState:            stateTerminated,
			expectedStateName:        stateTerminated,
			expectedStateOperational: false,
			expectedStateBatteryLvl:  defaultBatteryLvl,
			expectedErr:              nil,
		},
		{
			name:                     "hunterSetsToUnauthorizedState",
			givenOriginalState:       stateServiceMode,
			givenUserRole:            UserRoleHunter,
			givenNewState:            stateUnknown,
			expectedStateName:        stateServiceMode,
			expectedStateOperational: false,
			expectedStateBatteryLvl:  defaultBatteryLvl,
			expectedErr:              ErrUnauthorizedUserRole,
		},
		{
			name:                     "hunterSetsToInvalidStateTransition",
			givenOriginalState:       stateCollected,
			givenUserRole:            UserRoleHunter,
			givenNewState:            stateServiceMode,
			expectedStateName:        stateCollected,
			expectedStateOperational: true,
			expectedStateBatteryLvl:  defaultBatteryLvl,
			expectedErr:              ErrUnprocessableVehicleStateTransition,
		},
		{
			name:                     "hunterSetsToValidStateTransition",
			givenOriginalState:       stateBounty,
			givenUserRole:            UserRoleHunter,
			givenNewState:            stateCollected,
			expectedStateName:        stateCollected,
			expectedStateOperational: true,
			expectedStateBatteryLvl:  defaultBatteryLvl,
			expectedErr:              nil,
		},
		{
			name:                     "riderSetsToUnauthorizedState",
			givenOriginalState:       stateServiceMode,
			givenUserRole:            UserRoleRider,
			givenNewState:            stateReady,
			expectedStateName:        stateServiceMode,
			expectedStateOperational: false,
			expectedStateBatteryLvl:  defaultBatteryLvl,
			expectedErr:              ErrUnauthorizedUserRole,
		},
		{
			name:                     "riderSetsToInvalidStateTransition",
			givenOriginalState:       stateReady,
			givenUserRole:            UserRoleRider,
			givenNewState:            stateDropped,
			expectedStateName:        stateReady,
			expectedStateOperational: true,
			expectedStateBatteryLvl:  defaultBatteryLvl,
			expectedErr:              ErrUnprocessableVehicleStateTransition,
		},
		{
			name:                     "riderSetsToValidStateTransition",
			givenOriginalState:       stateReady,
			givenUserRole:            UserRoleRider,
			givenNewState:            stateRiding,
			expectedStateName:        stateRiding,
			expectedStateOperational: true,
			expectedStateBatteryLvl:  defaultBatteryLvl - 10,
			expectedErr:              nil,
		},
		{
			name:                     "adminToSteadyStateBatteryLow",
			givenOriginalState:       stateServiceMode,
			givenUserRole:            UserRoleAdmin,
			givenNewState:            stateBatteryLow,
			expectedStateName:        stateBatteryLow,
			expectedStateOperational: true,
			expectedStateBatteryLvl:  defaultBatteryLvl,
			expectedErr:              nil,
		},
		{
			name:                     "stateDroppedbatteryCharged",
			givenOriginalState:       stateCollected,
			givenUserRole:            UserRoleHunter,
			givenNewState:            stateDropped,
			expectedStateName:        stateDropped,
			expectedStateOperational: true,
			expectedStateBatteryLvl:  batteryMaxCap,
			expectedErr:              nil,
		},
	}

	for _, tc := range cases {
		t.Run(tc.name, func(t *testing.T) {
			scooter := NewScooter()
			scooter.stateName = tc.givenOriginalState
			scooter.operational = operationalState[tc.givenOriginalState]
			scooter.batteryLevel = defaultBatteryLvl

			err := scooter.SetState(tc.givenUserRole, tc.givenNewState)

			actualStateName := scooter.stateName
			actualStateOperational := scooter.operational
			actualStateBatteryLvl := scooter.batteryLevel

			assert.Equal(t, tc.expectedStateName, actualStateName)
			assert.Equal(t, tc.expectedStateOperational, actualStateOperational)
			assert.Equal(t, tc.expectedStateBatteryLvl, actualStateBatteryLvl)
			assert.Equal(t, tc.expectedErr, err)
		})
	}
}

func TestValidateStateTransition(t *testing.T) {
	t.Parallel()

	cases := []struct {
		name                    string
		givenCurrentState       string
		givenNewState           string
		expectedValidTransition bool
	}{
		{
			name:                    "ready->riding",
			givenCurrentState:       stateReady,
			givenNewState:           stateRiding,
			expectedValidTransition: true,
		},
		{
			name:                    "riding->ready",
			givenCurrentState:       stateRiding,
			givenNewState:           stateReady,
			expectedValidTransition: true,
		},
		{
			name:                    "bounty->collected",
			givenCurrentState:       stateBounty,
			givenNewState:           stateCollected,
			expectedValidTransition: true,
		},
		{
			name:                    "collected->dropped",
			givenCurrentState:       stateCollected,
			givenNewState:           stateDropped,
			expectedValidTransition: true,
		},
		{
			name:                    "dropped->ready",
			givenCurrentState:       stateDropped,
			givenNewState:           stateReady,
			expectedValidTransition: true,
		},
		{
			name:                    "foo->ready",
			givenCurrentState:       "foo",
			givenNewState:           stateReady,
			expectedValidTransition: false,
		},
		{
			name:                    "ready->dropped",
			givenCurrentState:       stateReady,
			givenNewState:           stateDropped,
			expectedValidTransition: false,
		},
	}

	for _, tc := range cases {
		t.Run(tc.name, func(t *testing.T) {
			scooter := NewScooter()
			scooter.stateName = tc.givenCurrentState

			actual := scooter.validateStateTransition(tc.givenNewState)

			assert.Equal(t, tc.expectedValidTransition, actual)
		})
	}
}

func TestChargeBattery(t *testing.T) {
	t.Parallel()

	cases := []struct {
		name                      string
		givenOriginalBatteryLevel int
		expectedBatteryLevel      int
		expectedStateName         string
		expectedStateOperational  bool
	}{
		{
			name:                      "bellowMaxLimit",
			givenOriginalBatteryLevel: 70,
			expectedBatteryLevel:      batteryMaxCap, // 100
			expectedStateName:         stateCollected,
			expectedStateOperational:  operationalState[stateCollected],
		},
	}

	for _, tc := range cases {
		t.Run(tc.name, func(t *testing.T) {
			scooter := NewScooter()
			scooter.batteryLevel = tc.givenOriginalBatteryLevel

			scooter.chargeBattery()

			actual := scooter.batteryLevel

			assert.Equal(t, tc.expectedBatteryLevel, actual)
		})
	}
}

func TestConsumeBattery(t *testing.T) {
	t.Parallel()

	cases := []struct {
		name                      string
		givenOriginalBatteryLevel int
		expectedBatteryLevel      int
		expectedStateName         string
		expectedStateOperational  bool
		expectedErr               error
	}{
		{
			name:                      "batteryLvlTooLow",
			givenOriginalBatteryLevel: 9, // 9 - batteryRunUsage = -1
			expectedBatteryLevel:      9,
			expectedStateName:         stateBounty,
			expectedStateOperational:  operationalState[stateBounty],
			expectedErr:               ErrVehicleBatteryLevelTooLow,
		},
		{
			name:                      "normalConsumption",
			givenOriginalBatteryLevel: 90,
			expectedBatteryLevel:      80,
			expectedStateName:         stateServiceMode, // Default state
			expectedStateOperational:  operationalState[stateServiceMode],
			expectedErr:               nil,
		},
		{
			name:                      "lowBattery",
			givenOriginalBatteryLevel: 29,
			expectedBatteryLevel:      19,
			expectedStateName:         stateBounty,
			expectedStateOperational:  operationalState[stateBounty],
			expectedErr:               nil,
		},
	}

	for _, tc := range cases {
		t.Run(tc.name, func(t *testing.T) {
			scooter := NewScooter()
			scooter.batteryLevel = tc.givenOriginalBatteryLevel

			err := scooter.consumeBattery()

			actualBatteryLvl := scooter.batteryLevel
			actualStateName := scooter.stateName
			actualStateOperational := scooter.operational

			assert.Equal(t, tc.expectedBatteryLevel, actualBatteryLvl)
			assert.Equal(t, tc.expectedStateName, actualStateName)
			assert.Equal(t, tc.expectedStateOperational, actualStateOperational)
			assert.Equal(t, tc.expectedErr, err)
		})
	}
}

func TestState(t *testing.T) {
	t.Parallel()

	expectedState := stateServiceMode // Default
	scooter := NewScooter()

	actual := scooter.State()

	assert.Equal(t, expectedState, actual)
}
