PROJECT_NAME := FLEETLIB

.PHONY: help
help:
	@echo "------------------------------------------------------------------------"
	@echo "${PROJECT_NAME}"
	@echo "------------------------------------------------------------------------"
	@grep -E '^[a-zA-Z0-9_/%\-]+:.*?## .*$$' $(MAKEFILE_LIST) | sort | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}'

build: ## compile packages and dependencies
	go build -race

.PHONY: dep
dep: ## install mock generator and linter
	# TODO: lock release
	go get -u  github.com/matryer/moq
	go get -u github.com/golangci/golangci-lint

.PHONY: generate
generate: ## generate interface mocks.
	go generate ./...

lint: ## run linter
	golangci-lint run

test: ## test packages
	go test -race -v ./...

tidy: ## add missing and remove unused modules
	go mod tidy

vendor: ## make vendored copy of dependencies
	go mod vendor
