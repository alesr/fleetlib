package fleetlib

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestUserRoleAllowedStates(t *testing.T) {
	t.Parallel()

	cases := []struct {
		name                         string
		givenUserRole                string
		expectedAllowedscooterStates []string
		expectedErr                  error
	}{
		{
			name:          "admin",
			givenUserRole: UserRoleAdmin,
			expectedAllowedscooterStates: []string{
				stateReady,
				stateBatteryLow,
				stateBounty,
				stateRiding,
				stateCollected,
				stateDropped,
				stateServiceMode,
				stateTerminated,
				stateUnknown,
			},
			expectedErr: nil,
		},
		{
			name:          "hunter",
			givenUserRole: UserRoleHunter,
			expectedAllowedscooterStates: []string{
				stateReady,
				stateRiding,
				stateBounty,
				stateCollected,
				stateDropped,
			},
			expectedErr: nil,
		},
		{
			name:          "rider",
			givenUserRole: UserRoleRider,
			expectedAllowedscooterStates: []string{
				stateReady,
				stateRiding,
			},
			expectedErr: nil,
		},
		{
			name:                         "unsupportedUserRole",
			givenUserRole:                "foo",
			expectedAllowedscooterStates: nil,
			expectedErr:                  ErrUnsupportedUserRole,
		},
	}

	for _, tc := range cases {
		t.Run(tc.name, func(t *testing.T) {

			actual, err := UserRoleAllowedVehicleStates(tc.givenUserRole)

			assert.Equal(t, tc.expectedAllowedscooterStates, actual)
			assert.Equal(t, tc.expectedErr, err)
		})
	}
}
